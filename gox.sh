#!/bin/bash
export ARCHES=(amd64 arm 386 mips mipsle s390x mips64le mips64)
export OS=(linux darwin freebsd windows netbsd openbsd)
export BINARIES=(olang olangcc opkg ops olangwc)
go get github.com/mitchellh/gox
if [ ! -d $(pwd)/build ];then
    mkdir -p $(pwd)/build
fi
cd build
rm -rf build/*
gox ..
gox ../olangcc/
gox ../opkg/
gox ../ops/
gox ../olangwc/

for i in ${OS[@]}; do
   for d in ${ARCHES[@]}; do
        mkdir -p ${i}_${d}/bin/
        if [ ${i} == "windows" ];then
            for f in ${BINARIES[@]};do
                if [ -f ${f}_${i}_${d}.exe ];then
                    mv -v ${f}_${i}_${d}.exe ${i}_${d}/bin/${f}.exe
                fi
            done
            if [ -d ${i}_${d}/ ];then
                zip -r ${i}_${d}.zip ${i}_${d} -x ${i}_${d}/.*
                rm -rfv ${i}_${d}
            fi
        else
            for f in ${BINARIES[@]};do
                if [ -f ${f}_${i}_${d} ];then
                    mv -v ${f}_${i}_${d} ${i}_${d}/bin/${f}
                fi
            done
            if [ -d ${i}_${d}/ ];then
                tar cvpjf ${i}_${d}.tar.bz2 ${i}_${d}/
                rm -rfv ${i}_${d}
            fi
        fi

   done
done
