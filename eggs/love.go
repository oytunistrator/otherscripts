package eggs

import(
  "strings"
)

type Love struct{
  Name string
}


func NewLove(name string) *Love{
  return &Love{Name: name}
}

func (r *Love) SayLove() string{
  ascii :="  ,d88b.d88b,                \n"
  ascii +="  88888888888                \n"
  ascii +="  `Y8888888Y'                \n" 
  ascii +="    `Y888Y'                  -{NAME}-      \n"
  ascii +="      `Y'   DEDICATED TO THE MOST BEAUTIFUL GIRL IN THE WORLD.                 \n"
  
  ascii = strings.Replace(ascii,"{NAME}",strings.ToUpper(r.Name), -1) 
  return ascii
}
