#!/bin/bash

WORKROOT=$(pwd)
cd ${WORKROOT}

# unzip go environment
if [ ! -d ${WORKROOT}/go ]; then
  go_env="go1.9.2.linux-amd64.tar.gz"
  wget -c https://redirector.gvt1.com/edgedl/go/go1.9.2.linux-amd64.tar.gz
  tar -zxf $go_env
  rm -rf $go_env
fi

# prepare PATH, GOROOT and GOPATH
export BUILD=${WORKROOT}/build
export GOROOT=${WORKROOT}/go
export GOPATH=${WORKROOT}/gopath
export BASESRC=${GOPATH}/src/github.com/olproject

if [ ! -d ${BUILD} ]
then
	mkdir -p ${BUILD}
fi
if [ ! -d ${GOPATH} ]
then
	mkdir -p ${GOPATH}
fi

export PATH=${WORKROOT}/go/bin:$PATH:${GOPATH}/bin

rm -rf ${GOPATH}/*

# needs
go get -v gopkg.in/src-d/go-git.v4
go get -v github.com/mattn/go-isatty
go get -v github.com/mattn/go-colorable
go get -v github.com/carmark/pseudo-terminal-go/terminal
go get -v golang.org/x/sys/windows
go get -v github.com/go-sql-driver/mysql
go get -v github.com/lib/pq
go get -v github.com/denisenkom/go-mssqldb
go get -v github.com/buger/jsonparser
go get -v github.com/clbanning/mxj
go get -v github.com/julienschmidt/httprouter
go get -v github.com/syndtr/goleveldb/leveldb
go get -v github.com/syndtr/goleveldb/leveldb/filter
go get -v github.com/syndtr/goleveldb/leveldb/opt
go get -v github.com/carmark/pseudo-terminal-go/terminal
go get -v github.com/mitchellh/gox

go get -v github.com/olproject/olang
go get -v github.com/olproject/opkg
go get -v github.com/olproject/ops

# cd ${BUILD}

upload_file()
{
    # config
    github_api_token="0bbb8642aa10dc9769ec04fa6d2f89bfdcc7194a"
    owner="olproject"
    repo="olang"
    tag="$1"
    filename="$2"
    
    # Define variables.
    GH_API="https://api.github.com"
    GH_REPO="$GH_API/repos/$owner/$repo"
    GH_TAGS="$GH_REPO/releases/tags/$tag"
    AUTH="Authorization: token $github_api_token"
    WGET_ARGS="--content-disposition --auth-no-challenge --no-cookie"
    CURL_ARGS="-LJO#"
    
    if [[ "$tag" == 'LATEST' ]]; then
      GH_TAGS="$GH_REPO/releases/latest"
    fi
    
    # Validate token.
    curl -o /dev/null -sH "$AUTH" $GH_REPO || { echo "Error: Invalid repo, token or network issue!";  exit 1; }
    
    # Read asset tags.
    response=$(curl -sH "$AUTH" $GH_TAGS)
    
    # Get ID of the asset based on given filename.
    eval $(echo "$response" | grep -m 1 "id.:" | grep -w id | tr : = | tr -cd '[[:alnum:]]=')
    [ "$id" ] || { echo "Error: Failed to get release id for tag: $tag"; echo "$response" | awk 'length($0)<100' >&2; exit 1; }
    
    # Upload asset
    echo "Uploading asset... "
    
    # Construct url
    GH_ASSET="https://uploads.github.com/repos/$owner/$repo/releases/$id/assets?name=$(basename $filename)"
    
    curl "$GITHUB_OAUTH_BASIC" --data-binary @"$filename" -H "Authorization: token $github_api_token" -H "Content-Type: application/octet-stream" $GH_ASSET
}

rm -rf ${BUILD}/*

if [ ! -d ${BASESRC}/olang/build/ ]
then
	mkdir -p ${BASESRC}/olang/build/
fi
cd ${BASESRC}/olang/build/
gox ..
cp -av * ${BUILD}/

if [ ! -d ${BASESRC}/opkg/build/ ]
then
	mkdir -p ${BASESRC}/opkg/build/
fi
cd ${BASESRC}/opkg/build/
gox ..
cp -av * ${BUILD}/

if [ ! -d ${BASESRC}/ops/build/ ]
then
	mkdir -p ${BASESRC}/ops/build/
fi
cd ${BASESRC}/ops/build/
gox ..
cp -av * ${BUILD}/

#gox ${BASESRC}/olang/
#gox ${BASESRC}/opkg/
#gox ${BASESRC}/ops/

cd ${BUILD}

ARCHES=(amd64 arm "386" mips mips64 mips64le mipsle s390x)
OS=(linux darwin freebsd windows netbsd openbsd)
BINARIES=(olang opkg ops)

for i in ${OS[@]}; do
   for d in ${ARCHES[@]}; do
        mkdir -p ${i}_${d}/bin/
        if [ ${i} == "windows" ];then
            for f in ${BINARIES[@]};do
                if [ -f ${f}_${i}_${d}.exe ];then
                    mv -v ${f}_${i}_${d}.exe ${i}_${d}/bin/${f}.exe
                fi
            done
            if [ -d ${i}_${d}/ ];then
                zip -r ${i}_${d}.zip ${i}_${d} -x ${i}_${d}/.*
                filesize=$(wc -c < ${i}_${d}.zip)
                if [ $filesize -le 500 ]; then
                    echo "[ERROR] File not builded: ${i}_${d}.zip"
                	rm -rfv ${i}_${d}.zip
                fi
                rm -rfv ${i}_${d}
            fi
        else
            for f in ${BINARIES[@]};do
                if [ -f ${f}_${i}_${d} ];then
                    mv -v ${f}_${i}_${d} ${i}_${d}/bin/${f}
                fi
            done
            if [ -d ${i}_${d}/ ];then
                tar cvpjf ${i}_${d}.tar.bz2 ${i}_${d}/
                filesize=$(wc -c < ${i}_${d}.tar.bz2)
                if [ $filesize -le 500 ]; then
                	echo "[ERROR] File not builded: ${i}_${d}.tar.bz2"
                	rm -rfv ${i}_${d}.tar.bz2
                fi
                rm -rfv ${i}_${d}
            fi
        fi
		
   done
done

if [ $? -ne 0 ];
then
    exit 1
fi
